#include <stdio.h>

const int arr1[3] = {1, 2, 3};
const int arr2[3] = {1, 2, 3};


int scalar_product(const int* firstArray, const int* secondArray, int length) {
    int sum = 0;

    for (size_t i = 0; i < 3; i++) {
        sum += firstArray[i] * secondArray[i];
    }
    return sum;
}


int main() {
    size_t length = sizeof arr1 / sizeof *arr1;
    printf("Скалярное произведение: %d\n", scalar_product(arr1, arr2, length));
    return 0;
}

