#include <stdio.h>

int is_prime(unsigned long n) {
    if (n <= 1) return 0;
    for (unsigned long i = 2; i <= (n / 2); i++) {
        if (n % i == 0) return 0;
    }
    return 1;
}

int main() {
    unsigned long n = NULL;
    printf("Введите число: ");
    scanf("%lu", &n);
    printf("%d\n", is_prime(n));
    return 0;
}

